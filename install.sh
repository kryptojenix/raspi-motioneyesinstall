#!/bin/bash

# this script modified from official debian instructions
# here: https://github.com/ccrisan/motioneye/wiki/Install-On-Debian
sudo apt update -y
sudo apt upgrade -y
sudo apt install -y motion ffmpeg v4l-utils python-pip python-dev python-setuptools curl libssl-dev libcurl4-openssl-dev libjpeg-dev libz-dev

# pip installs prerequisites automatically
# pip install motioneye

# if that fails install them one by one to catch errors

#     pip install Jinja2
#     pip --no-cache-dir install Pillow # fails with memory error, so --no-cache-dir. takes a long time
#     pip install pycurl
#     pip install tornado
mkdir ~/pip
cd ~/pip
wget https://bootstrap.pypa.io/get-pip.py
sudo ln -s /usr/local/bin/pip /usr/bin/
sudo python get-pip.py

# install motioneye including prerequisites 
pip --no-cache-dir install motioneye --user

# maybe should run with sudo (without --user)



# the following are for running as root
#sudo groupadd motioneye
#sudo mkdir -p /etc/motioneye
# these if running as root
#sudo cp /usr/local/share/motioneye/extra/motioneye.conf.sample /etc/motioneye/motioneye.conf
# sudo mkdir /var/lib/motioneye
# sudo chown root:motioneye /var/lib/motioneye
#sudo cp /usr/local/share/motioneye/extra/motioneye.systemd-unit-local /etc/systemd/system/motioneye.service




# set up home folder to run motioneye as unprivileged account
mkdir ~/motioneye
mkdir ~/motioneye/run
mkdir ~/motioneye/log
mkdir ~/motioneye/media


# using a supplied config (with user='meyeusr')
cp motioneye.conf ~/motioneye/

# TODO: edit the new config file
#     conf_path /home/youruser/motioneye
#     run_path /home/youruser/motioneye
#     log_path /home/youruser/motioneye
#     media_path /home/youruser/motioneye

# link ~/.local/bin/meyectl to somewhere in path
sudo ln -s ~/.local/bin/meyectl /usr/bin/meyectl

 
# TODO: edit the service file at /etc/systemd/system/motioneye.service
#     [Service]
#     ExecStart=/usr/bin/meyectl startserver -c /home/youruser/motioneye/motioneye.conf
#     User=youruser

# using a supplied service file (with user='meyeusr')
sudo cp motioneye.service /etc/systemd/system/


sudo systemctl daemon-reload
sudo systemctl start motioneye
# TODO: if success
sudo systemctl enable motioneye

# Now check the web interface on another compter and log in as admin with no password and configure cameras
echo "go to MEYE_IP:8765 and log in as admin (no password)"



# TODO: if installed, run the update
#     pip install motioneye --upgrade
#     systemctl restart motioneye
