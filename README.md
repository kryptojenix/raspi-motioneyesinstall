# Raspi-MotionEyesInstall

Some scripts to automate the install of MotionEyes on Raspberry Pi


1. write the raspbian lite image to SD card and boot from it
2. log in as pi:raspberry
3. configure a static IP or use DHCP
    $nano /etc/network/interfaces
    
        auto eth0
        allow hotplug eth0
        device eth0 inet dhcp
    $sudo systemctl restart networking
    $sudo ifup -a
    $ip a
    
    3.2 note your IP address
    
    3.3 If there were no errors & you can proceed. 
    
    
4. $sudo raspi-config
    4.1 set a simple password for the "pi" user.
        This account will be removed for security in the final steps.
    
    4.2 set a hostname
    4.3 set locale to en_AU.UTF-8 & timezone & keyboard
    4.4 enable the camera and ssh
    4.5 expand the filesystem
    
    
6. reboot (and hopefully all those settings were applied on boot)
    if not, the next step might fail. keep the monitor and keyboard plugged into your RPi
7. log in via ssh
    $ssh pi@192.168.0.2
8. install python3 & git
9. create 2 new users and home folders. First for the motioneye process owner
    the service will be run as a normal user with limited system access. Second for yourself to ssh into the system (a replacement for "pi")
    9.1 $sudo adduser [meyeuser]
    9.2 $sudo adduser [youruser]
10.Add the new users to the following groups: video, GPIO,
    10.1 $
11. log in as the new motioneyes user
12. create a python folder & change into it
13. $git clone https://gitlab.com/kryptojenix/powerLEDs.git
14. $git clone https://gitlab.com/kryptojenix/raspi-motioneyesinstall.git



if pip errors installing motioneye, remove the debian version of pip and use the pip version of pip
+https://stackoverflow.com/questions/37495375/python-pip-install-throws-typeerror-unsupported-operand-types-for-retry
